package br.ucsal.bes20211.testequalidade.aula07.business;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20211.testequalidade.aula07.builder.AlunoBuilder;
import br.ucsal.bes20211.testequalidade.aula07.domain.Aluno;
import br.ucsal.bes20211.testequalidade.aula07.domain.Disciplina;
import br.ucsal.bes20211.testequalidade.aula07.domain.Matricula;
import br.ucsal.bes20211.testequalidade.aula07.exception.MatriculaException;

public class MatriculaBCTest {

	@Test
	void testarPorpostaMatriculaValida() {
		// Aluno aluno1 = AlunoMother.umAlunoAtivoMaiorIdade();
		// Aluno aluno1 = AlunoBuilder.umAluno().ativo().nascidoEm(LocalDate.of(2000, 1,
		// 1)).build();
		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().nascidoEm(LocalDate.of(2000, 1, 1)).build();

		// O trabalho com Builder também deve ser feito para a disciplina
		Disciplina disciplina1 = new Disciplina();

		Matricula matriculaAtual = MatriculaBC.proporMatricula(aluno1, disciplina1);

		// O trabalho com Builder também deve ser feito para a matrícula
		Matricula matriculaEsperada = new Matricula();

		Assertions.assertEquals(matriculaEsperada, matriculaAtual);
	}

	@Test
	void testarPorpostaMatriculaNaoValidaAlunoTrancado() {
		// Aluno aluno1 = AlunoMother.umAlunoTrancado();
		// Aluno aluno1 = AlunoBuilder.umAluno().trancado().nascidoEm(LocalDate.of(2000,
		// 5, 12)).build();
		Aluno aluno1 = AlunoBuilder.umAlunoTrancado().nascidoEm(12, 5, 2000).build();

		// O trabalho com Builder também deve ser feito para a disciplina
		Disciplina disciplina1 = new Disciplina();

		Assertions.assertThrows(MatriculaException.class, () -> MatriculaBC.proporMatricula(aluno1, disciplina1));
	}

	@Test
	void ilustrarCustomizacaoBuilder() {
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno().comMatricula(345).comNome("Maria")
				.comEmail("maria@ucsal.br");
		Aluno aluno1 = alunoBuilder.build();
		Aluno aluno2 = alunoBuilder.mas().comMatricula(789).build();
		Aluno aluno3 = alunoBuilder.mas().comNome("Pedro").build();
		Aluno aluno4 = alunoBuilder.mas().comEmail("jose@br").build();
		System.out.println("aluno1=" + aluno1); // 345 Maria maria@ucsal.br
		System.out.println("aluno2=" + aluno2); // 789 Maria maria@ucsal.br
		System.out.println("aluno3=" + aluno3); // 345 Pedro maria@ucsal.br
		System.out.println("aluno4=" + aluno4); // 345 Maria jose@br
	}
}
