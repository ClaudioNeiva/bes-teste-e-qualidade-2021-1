package br.ucsal.bes20211.testequalidade.aula07.builder;

import java.time.LocalDate;

import br.ucsal.bes20211.testequalidade.aula07.domain.Aluno;
import br.ucsal.bes20211.testequalidade.aula07.domain.SituacaoAlunoEnum;

// NÃO utilizamos mais o AlunoMother, pois o AlunoBuilder é mais flexível. 
// Porém, levamos o conceito de instâncias padronizadas para dentro do AlunoBuilder.
public class AlunoMother {

	public static Aluno umAlunoCadastroCompleto() {
		Aluno aluno = new Aluno(1, "Claudio Neiva");
		aluno.setDataNascimento(LocalDate.of(2010, 1, 1));
		aluno.setEmail("claudio@ucsal.br");
		aluno.setSituacao(SituacaoAlunoEnum.ATIVO);
		return aluno;
	}

	public static Aluno umAlunoAtivo() {
		Aluno aluno = umAlunoCadastroCompleto();
		aluno.setSituacao(SituacaoAlunoEnum.ATIVO);
		return aluno;
	}

	public static Aluno umAlunoTrancado() {
		Aluno aluno = umAlunoCadastroCompleto();
		aluno.setSituacao(SituacaoAlunoEnum.TRANCADO);
		return aluno;
	}

	public static Aluno umAlunoAtivoMaiorIdade() {
		Aluno aluno = umAlunoAtivo();
		aluno.setDataNascimento(LocalDate.of(2000, 1, 1));
		return aluno;
	}

}
