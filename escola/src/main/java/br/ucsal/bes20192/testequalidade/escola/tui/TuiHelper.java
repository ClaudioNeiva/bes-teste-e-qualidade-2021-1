package br.ucsal.bes20192.testequalidade.escola.tui;

import java.util.Scanner;

public class TuiHelper {

	public Scanner scanner = new Scanner(System.in);

	public String obterNomeCompleto() {
		System.out.println("Informe o nome:");
		String nome = scanner.nextLine();
		System.out.println("Informe o sobrenome:");
		String sobrenome = scanner.nextLine();
		return nome + " " + sobrenome;
	}

	public void exibirMensagem(String mensagem) {
		System.out.print("Bom dia! ");
		System.out.println(mensagem);
	}

}
