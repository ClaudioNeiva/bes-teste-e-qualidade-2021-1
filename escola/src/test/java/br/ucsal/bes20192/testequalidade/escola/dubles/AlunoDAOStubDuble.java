package br.ucsal.bes20192.testequalidade.escola.dubles;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOStubDuble extends AlunoDAO {

	private Integer anoNascimento;

	public void definirAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public void salvar(Aluno aluno) {
	}
	
	@Override
	public void excluirTodos() {
	}
	
	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		Aluno aluno = new Aluno();
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

}
