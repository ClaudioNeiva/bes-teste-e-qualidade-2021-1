package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	private AlunoBO alunoBO;
	private AlunoDAO alunoDAO;
	private DateHelper dateHelper;

	@BeforeEach
	void setup() {
		alunoDAO = new AlunoDAO();
		dateHelper = new DateHelper();
		alunoBO = new AlunoBO(alunoDAO, dateHelper);
		alunoDAO.excluirTodos();
	}

	@AfterEach
	void teardown() {
		alunoDAO.excluirTodos();
	}

	@Test
	void testarCalcularIdade1() {
		// FIXME Será necessário alterar a data de sistema para garantir o funcionamento
		// deste teste ao longo do tempo.

		// Ano atual = 2021!
		Integer anoNascimento = 2000;
		Integer idadeEsperada = 21;

		Integer matricula = 10;
		Aluno aluno = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(anoNascimento).build();
		alunoDAO.salvar(aluno);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarAtualizarAlunoAtivo() {
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().nascidoEm(2000).build();

		alunoBO.atualizar(alunoEsperado);

		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(alunoEsperado.getMatricula());

		Assertions.assertEquals(alunoEsperado, alunoAtual);

	}

}
