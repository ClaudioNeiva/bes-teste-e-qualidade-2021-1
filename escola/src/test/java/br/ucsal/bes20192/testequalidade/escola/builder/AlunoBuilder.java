package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {

	private static final Integer DEFAULT_MATRICULA = 1;
	private static final String DEFAULT_NOME = "Claudio Neiva";
	private static final Integer DEFAULT_ANO_NASCIMENTO = null;
	private static final SituacaoAluno DEFAULT_SITUACAO = null;

	private Integer matricula = DEFAULT_MATRICULA;
	private String nome = DEFAULT_NOME;
	private Integer anoNascimento = DEFAULT_ANO_NASCIMENTO;
	private SituacaoAluno situacao = DEFAULT_SITUACAO;

	private AlunoBuilder() {
	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}

	public static AlunoBuilder umAlunoAtivo() {
		return new AlunoBuilder().ativo();
	}

	public static AlunoBuilder umAlunoCancelado() {
		return new AlunoBuilder().cancelado();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder nascidoEm(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder naSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
		return this;
	}

	public AlunoBuilder ativo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder cancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}

	public AlunoBuilder mas() {
		return new AlunoBuilder().comMatricula(matricula).comNome(nome).nascidoEm(anoNascimento).naSituacao(situacao);
	}

	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setAnoNascimento(anoNascimento);
		aluno.setSituacao(situacao);
		return aluno;
	}

}
