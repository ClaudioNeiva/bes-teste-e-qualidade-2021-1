package br.ucsal.bes20192.testequalidade.escola.testescomdublemanual;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.business.AlunoBO;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.dubles.AlunoDAOMockDuble;
import br.ucsal.bes20192.testequalidade.escola.dubles.DateHelperStubDuble;

public class AlunoBO3Test {

	private AlunoBO alunoBO;
	private AlunoDAOMockDuble alunoDAOMock;
	private DateHelperStubDuble dateHelperStub;

	@BeforeEach
	void setup() {
		// Pra o teste do AlunoBO.calcularIdade seja unitário, o AlunoDAO e o
		// DateHelper deve usar dublês!
		alunoDAOMock = new AlunoDAOMockDuble();	
		dateHelperStub = new DateHelperStubDuble();
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperStub);
	}

	@Test
	void testarCalcularIdade() {
		Integer anoAtual = 2021;
		Integer anoNascimentoAtual = 2000;
		Integer idadeEsperada = 21;

		dateHelperStub.definirAnoAtual(anoAtual);
		alunoDAOMock.definirAnoNascimento(anoNascimentoAtual);
		
		// O método calcularIdade é chamado QUERY.
		Integer idadeAtual = alunoBO.calcularIdade(10);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}
	
	@Test
	void testarAtualizarAlunoAtivo() {
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();

		// O método atualizar é chamado COMMAND.
		alunoBO.atualizar(alunoEsperado);

		alunoDAOMock.verificarChamadasSalvar(alunoEsperado, 1);
	}


}
