package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOTest3 {

	private AlunoBO alunoBO;
	private AlunoDAO alunoDAOMock;
	private DateHelper dateHelperMock;
	private Integer anoAtual = 2021;
	private Aluno aluno;
	private Integer matricula = 1;

	@BeforeEach
	void setup() {
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		dateHelperMock = Mockito.mock(DateHelper.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperMock);

		// Configurar o dateHelperMock para que retorne 2021 na chamada ao
		// obterAnoAtual()
		Mockito.when(dateHelperMock.obterAnoAtual()).thenReturn(anoAtual);
		
		aluno = AlunoBuilder.umAluno().comMatricula(matricula).build();
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno);
	}

	@Test
	void testarCalcularIdade() {
		aluno.setAnoNascimento(2000);
		Integer idadeEsperada = 21;

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarCalcularIdade2() {
		aluno.setAnoNascimento(1996);
		Integer idadeEsperada = 25;

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarAtualizarAlunoAtivo() {
		aluno.setSituacao(SituacaoAluno.ATIVO);
		
		alunoBO.atualizar(aluno);

		// Verificar se foi chamado para alunoDAOMock o método salvar,
		// passando como parâmetro o objeto alunoEsperado.
		Mockito.verify(alunoDAOMock).salvar(aluno);
	}

}
