package br.ucsal.bes20192.testequalidade.escola.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBO2Test {

	private AlunoBO alunoBO;
	private AlunoDAO alunoDAOMock;
	private DateHelper dateHelperMock;
	private Integer anoAtual = 2021;

	@BeforeEach
	void setup() {
		alunoDAOMock = mock(AlunoDAO.class);
		dateHelperMock = mock(DateHelper.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperMock);

		// Configurar o dateHelperMock para que retorne 2021 na chamada ao
		// obterAnoAtual()
		when(dateHelperMock.obterAnoAtual()).thenReturn(anoAtual);
	}

	@Test
	void testarCalcularIdade() {
		Integer matricula = 1;
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(2000).build();

		Integer idadeEsperada = 21;

		// Configurar o alunoDAOMock para retornar o aluno1, quando for chamado o
		// encontrarPorMatricula(matricula)
		when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno1);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarCalcularIdade2() {
		Integer matricula = 1;
		Aluno aluno1 = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(1996).build();

		Integer idadeEsperada = 25;

		when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno1);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarAtualizarAlunoAtivo() {
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();

		alunoBO.atualizar(alunoEsperado);

		// Verificar se foi chamado para alunoDAOMock o método salvar,
		// passando como parâmetro o objeto alunoEsperado.
		verify(alunoDAOMock).salvar(alunoEsperado);
	}

}
