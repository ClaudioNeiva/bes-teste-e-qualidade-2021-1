package br.ucsal.bes20192.testequalidade.escola.testescomdublemanual;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.business.AlunoBO;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.dubles.AlunoDAOFakeDuble;
import br.ucsal.bes20192.testequalidade.escola.dubles.DateHelperStubDuble;

public class AlunoBOTest {

	private AlunoBO alunoBO;
	private AlunoDAOFakeDuble alunoDAOFake;
	private DateHelperStubDuble dateHelperStub;

	@BeforeEach
	void setup() {
		// Pra o teste do AlunoBO.calcularIdade seja unitário, o AlunoDAO e o
		// DateHelper deve usar dublês!
		alunoDAOFake = new AlunoDAOFakeDuble();
		dateHelperStub = new DateHelperStubDuble();
		alunoBO = new AlunoBO(alunoDAOFake, dateHelperStub);
	}

	@Test
	void testarCalcularIdade() {
		Integer anoAtual = 2021;
		Integer anoNascimento = 2000;
		Integer idadeEsperada = 21;

		dateHelperStub.definirAnoAtual(anoAtual);

		Integer matricula = 10;
		Aluno aluno = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(anoNascimento).build();
		alunoDAOFake.salvar(aluno);

		// O método calcularIdade é chamado QUERY.
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	@Test
	void testarAtualizarAlunoAtivo() {
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();

		// O método atualizar é chamado COMMAND.
		alunoBO.atualizar(alunoEsperado);

		Aluno alunoAtual = alunoDAOFake.encontrarPorMatricula(alunoEsperado.getMatricula());

		Assertions.assertEquals(alunoEsperado, alunoAtual);
	}

}
