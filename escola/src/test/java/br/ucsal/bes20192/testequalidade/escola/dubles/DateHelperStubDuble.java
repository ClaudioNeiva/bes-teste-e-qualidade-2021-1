package br.ucsal.bes20192.testequalidade.escola.dubles;

import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class DateHelperStubDuble extends DateHelper {

	private Integer anoAtual;

	public void definirAnoAtual(Integer anoAtual) {
		this.anoAtual = anoAtual;
	}

	@Override
	public Integer obterAnoAtual() {
		return anoAtual;
	}

}
