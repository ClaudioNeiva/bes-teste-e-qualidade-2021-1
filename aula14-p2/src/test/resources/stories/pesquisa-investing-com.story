Narrativa:
Como um investidor
desejo pesquisar ativos por código
de modo que possa saber o nome do mesmo

Cenário: Pesquisar por COGN3 no Chrome
Dado que estou no GoogleChrome
E estou no site http://br.investing.com
Quando informo COGN3 no campo de pesquisa
Então é apresentado na página o conteúdo Cogna Educacao SA
E o browser é fechado

Cenário: Pesquisar por COGN3 no Firefox
Dado que estou no Firefox
E estou no site http://br.investing.com
Quando informo COGN3 no campo de pesquisa
Então é apresentado na página o conteúdo Cogna Educacao SA
E o browser é fechado

Cenário: Pesquisar por USIM5 no Chrome
Dado que estou no GoogleChrome
E estou no site http://br.investing.com
Quando informo USIM5 no campo de pesquisa
Então é apresentado na página o conteúdo Usiminas
E o browser é fechado