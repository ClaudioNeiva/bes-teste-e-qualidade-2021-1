package br.ucsal.bes20211.testequalidade.aula14;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PesquisaInvestingComSteps {

	private WebDriver driver;

	@Given("estou no $nomeBrowser")
	public void abrirBrowser(String nomeBrowser) {
		if (nomeBrowser.equalsIgnoreCase("GoogleChrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver-90");
			driver = new ChromeDriver();
		} else if (nomeBrowser.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver-0.26");
			driver = new FirefoxDriver();
		} else {
			throw new RuntimeException("Não existe implementação de teste para o browser " + nomeBrowser);
		}
	}

	@Given("estou no site $url")
	public void abrirPagina(String url) {
		driver.get(url);
	}

	@When("informo $valorPesquisa no campo de pesquisa")
	public void informarValorPesquisa(String valorPesquisa) {
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys(valorPesquisa + Keys.ENTER);
	}

	@Then("é apresentado na página o conteúdo $fragmentoConteudoEsperado")
	public void verificarFragmentoConteudo(String fragmentoConteudoEsperado) throws InterruptedException {
		Thread.sleep(2000);
		String conteudo = driver.getPageSource();
		Assert.assertTrue(conteudo.contains(fragmentoConteudoEsperado));
	}

	@Then("o browser é fechado")
	public void fecharBrowser() {
		driver.close();
	}

}
