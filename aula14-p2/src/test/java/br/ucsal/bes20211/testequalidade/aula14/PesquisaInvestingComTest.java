package br.ucsal.bes20211.testequalidade.aula14;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

public class PesquisaInvestingComTest extends StoriesAbtract {
	
	@Override
	protected List<String> storyPaths() {
		return new StoryFinder().findPaths(CodeLocations.codeLocationFromClass(this.getClass()),
			Arrays.asList("**/pesquisa-investing-com.story"), Arrays.asList(""));
	}
	
	@Override
	public InjectableStepsFactory stepsFactory() {
			return new InstanceStepsFactory(configuration(), new PesquisaInvestingComSteps());
	}
	
}
