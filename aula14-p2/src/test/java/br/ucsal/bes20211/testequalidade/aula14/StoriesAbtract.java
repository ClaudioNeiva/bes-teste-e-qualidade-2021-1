package br.ucsal.bes20211.testequalidade.aula14;

import java.util.Locale;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.MarkUnmatchedStepsAsPending;

public abstract class StoriesAbtract extends JUnitStories {
	
	@Override
	public Configuration configuration() {
		
		Keywords keywords = new LocalizedKeywords(new Locale("pt"));
		
		return new MostUsefulConfiguration()
				.useKeywords(keywords)
				.useStoryParser(new RegexStoryParser(keywords))
				.useStepCollector(new MarkUnmatchedStepsAsPending(keywords))
				.useStoryReporterBuilder(
						new StoryReporterBuilder().withDefaultFormats(). 
						withFormats(Format.CONSOLE, Format.TXT, Format.HTML));
	}
	
}
