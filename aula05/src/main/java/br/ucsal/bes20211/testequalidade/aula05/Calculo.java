package br.ucsal.bes20211.testequalidade.aula05;

import java.util.Scanner;

public class Calculo {

	private Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Calculo calculo = new Calculo();
		calculo.apresentarResultado(calculo.calcularSoma());
	}

	public int calcularSoma() {
		int num1;
		int num2;

		// Entrada de dados 1.
		System.out.println("Informe o primeiro número:");
		num1 = scanner.nextInt();

		// Entrada de dados 2.
		System.out.println("Informe o segundo número:");
		num2 = scanner.nextInt();

		return num1 + num2;
	}

	public void apresentarResultado(int x) {
		String mensagem = "O resultado da operação foi:" + x;
		System.out.println(mensagem);
	}

}
