package br.ucsal.bes20211.testequalidade.aula05;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoTest {

	private static final String LINE_SEPARTOR = System.lineSeparator();

	@Test
	void testCalcularSoma() {
		ByteArrayInputStream inFake = new ByteArrayInputStream("45\n8".getBytes());

		System.setIn(inFake);

		Calculo calculo = new Calculo();
		int resultadoAtual = calculo.calcularSoma();

		int resultadoEsperado = 53;

		Assertions.assertEquals(resultadoEsperado, resultadoAtual);
	}

	@Test
	void testApresentarResultado() {
		ByteArrayOutputStream outFake = new ByteArrayOutputStream();

		System.setOut(new PrintStream(outFake));

		Calculo calculo = new Calculo();
		calculo.apresentarResultado(50);
		String resultadoAtual = outFake.toString();

		String resultadoEsperado = "O resultado da operação foi:50" + LINE_SEPARTOR;
		Assertions.assertEquals(resultadoEsperado, resultadoAtual);
	}

}
