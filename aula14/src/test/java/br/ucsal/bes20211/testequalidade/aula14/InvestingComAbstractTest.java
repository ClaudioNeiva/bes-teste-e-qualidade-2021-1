package br.ucsal.bes20211.testequalidade.aula14;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class InvestingComAbstractTest {

	protected WebDriver driver;

	@AfterEach
	public void teardown() {
		driver.close();
	}

	@Test
	public void testarPesquisa() throws InterruptedException {
		// Abrir página do Investing.com
		driver.get("http://br.investing.com");

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);

		// Se fosse uma requisição Ajax, teríamos que esperar o fim da mesma. Uma forma
		// de fazer isso é colocar uma pausa fixa (o que não é interessante), esperar um
		// elemento estar disponível na página ou ficar numa espera ocupada pelo fim da
		// requisição (lembre-se de configurar um timeout).
		Thread.sleep(2000);

		// Obter o conteúdo da página
		String conteudo = driver.getPageSource();

		// Verificar se retorno inclui "Cogna Educação"
		Assertions.assertTrue(conteudo.contains("Cogna Educacao SA"));
	}

}
