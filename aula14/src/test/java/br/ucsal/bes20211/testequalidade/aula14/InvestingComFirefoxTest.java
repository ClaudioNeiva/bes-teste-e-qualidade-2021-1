package br.ucsal.bes20211.testequalidade.aula14;

import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InvestingComFirefoxTest extends InvestingComAbstractTest {

	@BeforeEach
	public void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver-0.26");
		driver = new FirefoxDriver();
	}

}
