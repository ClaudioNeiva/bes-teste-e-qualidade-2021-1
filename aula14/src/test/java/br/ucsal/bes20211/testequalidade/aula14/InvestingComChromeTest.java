package br.ucsal.bes20211.testequalidade.aula14;

import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.chrome.ChromeDriver;

public class InvestingComChromeTest extends InvestingComAbstractTest {

	@BeforeEach
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver-90");
		driver = new ChromeDriver();
	}
	
}
