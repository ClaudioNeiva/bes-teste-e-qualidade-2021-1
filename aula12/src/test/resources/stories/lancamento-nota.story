Narrativa:
Como um professor
desejo informar as duas notas de um aluno
de modo que possa saber se o mesmo foi aprovado

Cenário: Aluno aprovado com nota superior à média (7.0) 
Dado que um aluno está matriculado na disciplina
Quando informo a nota 8
E informo a nota 9
Então a situação do aluno é Aprovado

Cenário: Aluno aprovado com nota igual à média (7.0)
Dado que um aluno está matriculado na disciplina
Quando informo a nota 8
E informo a nota 6
Então a situação do aluno é Aprovado

Cenário: Aluno reprovado com nota menor que média (7.0)
Dado que um aluno está matriculado na disciplina
Quando informo a nota 6
E informo a nota 4
Então a situação do aluno é Reprovado
