package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class ExemploAnotacoesJUnitTest {

	@BeforeAll
	static void setupClass() {
		System.out.println("setupClass");
	}

	@AfterAll
	static void tearDownClass() {
		System.out.println("tearDownClass");
	}

	@BeforeEach
	void setup() {
		System.out.println("	setup");
	}

	@AfterEach
	void tearDown() {
		System.out.println("	tearDown");
	}

	@Test
	void test1() {
		System.out.println("		test1");
	}

	@Test
	@Disabled
	// FIXME Necessário refatorar pode mudança no bla bla bla.
	void test2() {
		System.out.println("		test2");
	}

	@Test
	void test3() {
		System.out.println("		test3");
	}

}
