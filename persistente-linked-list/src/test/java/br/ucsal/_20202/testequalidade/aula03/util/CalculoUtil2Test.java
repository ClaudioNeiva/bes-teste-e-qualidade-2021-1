package br.ucsal._20202.testequalidade.aula03.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CalculoUtil2Test {

	@ParameterizedTest(name = "{index} calcularFatorial({0})")
	@CsvSource(value = { "0,1", "1,1", "5,120", "6,720" })
	void testarFatorial(Integer n, Long fatorialEsperado) {
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
