package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

public class PersitenteLinkedListTest {

	private static final String TAB_NAME_LIST = "lista";

	@Test
	void testarPersistence1Elemento() {
	}
	
	@Test
	void testarPersistence3Elementos()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {

		Assumptions.assumeTrue(DbUtil.isConnectionValid());
		
		Connection connection = DbUtil.getConnection();

		Long idLista = 1L;

		PersistentList<String> nomesPersistent = new PersitenteLinkedList<>();
		
		nomesPersistent.delete(idLista, connection, TAB_NAME_LIST);

		nomesPersistent.add("antonio");
		nomesPersistent.add("claudio");
		nomesPersistent.add("neiva");

		nomesPersistent.persist(idLista, connection, TAB_NAME_LIST);

		PersistentList<String> nomesPersistentAtual = new PersitenteLinkedList<>();
		nomesPersistentAtual.load(idLista, connection, TAB_NAME_LIST);

		assertAll(
			() -> assertEquals("antonio", nomesPersistentAtual.get(0)),
			() -> assertEquals("claudio", nomesPersistentAtual.get(1)),
			() -> assertEquals("neiva", nomesPersistentAtual.get(2)),
			() -> assertEquals(3, nomesPersistentAtual.size()));

		nomesPersistent.delete(idLista, connection, TAB_NAME_LIST);

	}

}
