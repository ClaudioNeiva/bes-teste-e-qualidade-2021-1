package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

public class LinkedListTest {

	private List<String> nomes;

	@BeforeEach
	void setup() {
		nomes = new LinkedList<>();
	}

	@Test
	@DisplayName("Inclusão de 1 elemento na lista")
	void testarAdd1Elemento() throws InvalidElementException {
		nomes.add("claudio");
		assertEquals("claudio", nomes.get(0));
	}

	@Test
	@DisplayName("Inclusão de 3 elementos na lista")
	void testarAdd3Elementos() throws InvalidElementException {
		nomes.add("claudio");
		nomes.add("antonio");
		nomes.add("neiva");
		assertAll( 
			() -> assertEquals("claudio", nomes.get(0)),
			() -> assertEquals("antonio", nomes.get(1), "O segundo nome deveria ser antonio."),
			() -> assertEquals("neiva", nomes.get(2))
		);
	}
	
	@Test
	void testarAddElementoNulo() throws InvalidElementException {
		InvalidElementException exceptionAtual = assertThrows(InvalidElementException.class, () -> nomes.add(null));
		assertEquals("The element can't be null.", exceptionAtual.getMessage());
	}

}
