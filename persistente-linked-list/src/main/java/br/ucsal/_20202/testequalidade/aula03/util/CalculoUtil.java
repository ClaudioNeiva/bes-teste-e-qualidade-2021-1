package br.ucsal._20202.testequalidade.aula03.util;

public class CalculoUtil {

	private CalculoUtil() {
	}

	public static Long calcularFatorial(Integer n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
