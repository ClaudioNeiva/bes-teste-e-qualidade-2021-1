package br.ucsal.ads20211.testequalidade.atividade02;

public class CalculoUtil {

	public static Long calcularFatorial(int n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
